package se.erikwiberg.reflection;

import static org.junit.Assert.*;

import java.lang.reflect.ParameterizedType;

import org.junit.Test;

/** 
 * Confirm we can get the generic type (at runtime) from a class ChildClass that extends some class ParentClass<T>. 
 * This is useful in a number of cases where we, for example, have some shared functionality in an abstract base class
 * but we are dependent on some type information that can't be known (without reflection) from within the abstract class.
 * A concrete example would be a generic DAO with common CRUD operations, but where the class name must be known so as to 
 * access a @NamedQuery named after the pattern "className.findAll" 
 *  see https://gitlab.com/erik-wiberg-87/CLUB-mvn/blob/master/src/main/java/club/DAO/GenericCrudDao.java
 */
public class ReflectionTest {

	@Test
	public void runtimeGenericTypeExtractionTest() {
		// Create concrete implementations with different type arguments
		final GenericAbstractClass<String> concreteStringImpl = new ConcreteStringImplementation();
		final GenericAbstractClass<Long> concreteLongImpl = new ConcreteLongImplementation();
		final GenericAbstractClass<Integer> concreteIntegerImpl = new ConcreteIntegerImplementation();
		final GenericAbstractClass<Boolean> concreteBooleanImpl = new ConcreteBooleanImplementation();
		
		// Test the getGenericClass() method
		final String msg = "The classes should match!";
		assertEquals(msg, String.class, concreteStringImpl.getGenericClass());
		assertEquals(msg, Long.class, concreteLongImpl.getGenericClass());
		assertEquals(msg, Integer.class, concreteIntegerImpl.getGenericClass());
		assertEquals(msg, Boolean.class, concreteBooleanImpl.getGenericClass());
	}
	
	/**
	 * A simple abstract class  
	 */
	public class GenericAbstractClass<T> {
		
		public Class<T> getGenericClass() {
		/*
		 *  -- Step-by-step --   
		 * 1) Get the class of "this" (which will the implementing, concrete, class. Eg. some class like: public ClassA extends GenericAbstractClass<Long>)
		 * 2) Get the generic superclass (ie. THIS class, GenericAbstractClass)
		 * 3) Cast it to java.lang.reflect.ParameterizedType, which (somehow, through "reflection", can be used to ask further questions.. ) 
		 * 4) Get an array of the actual type arguments (ie. the types specified in the concrete class, like the type Long in "public ClassA extends GenericAbstractClass<Long>"
		 * 5) In this particular case, there's just one. So just return the first (and only) class in the array, ie index 0.
		 * 6) Finally, cast the java.lang.reflect.Type to a Class
		 * 	  In our example, "public ClassA extends GenericAbstractClass<Long>", the class will hopefully be java.lang.Long
		 */
			return (Class<T>) ((ParameterizedType) this.getClass().getGenericSuperclass()).getActualTypeArguments()[0];
		}
	
	}
	/**
	 * Different concrete implementations of GenericAbstractClass<T>. To be used to see if we can extract the class of T at runtime
	 */
	public class ConcreteStringImplementation extends GenericAbstractClass<String> {};
	public class ConcreteLongImplementation extends GenericAbstractClass<Long> {};
	public class ConcreteIntegerImplementation extends GenericAbstractClass<Integer> {};
	public class ConcreteBooleanImplementation extends GenericAbstractClass<Boolean> {};

}
