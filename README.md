# Java Generics & Reflection

Java generics are a great tool, but sometime you want to know the type of T at runtime. But there is no simple way of writing the equivalent of "return T" to get the class of T.
One solution is to use Java's reflection API. See [ReflectionTest.java](src/test/java/se/erikwiberg/reflection/ReflectionTest.java )

I'm basically just trying out this "simple" line of code `(Class<T>) ((ParameterizedType) this.getClass().getGenericSuperclass()).getActualTypeArguments()[0];`

**Disclaimer**: I'm not the "inventor" of this neat piece of code, I'm just trying to make sense of it, for future reference if nothing else. 